// Beatriz Durán y Gonzalo Inostroza
import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner tcld = new Scanner(System.in);

        System.out.println("Ingrese el nombre del archivo a leer, se tiene un archivo de prueba llamado \"Prueba.txt\" ");
        System.out.print(">");
        String archivoNombre = tcld.nextLine();


        Index indiceAlfabetico = new Index();
        int numeroPagina = 1;
        Scanner lectorArchivo;

        try {
            lectorArchivo = new Scanner(new File(archivoNombre));
            lectorArchivo.useDelimiter("\\|");
        } catch (Exception e) {
            System.out.println("Hubo un error al leer el archivo");
            System.exit(-1);
            return; // JAVA LITERALMENTE LA FUNCION EXIT DE PARTE DE LA LIBRERIA BASE COMO TE PONES A LLORAR POR ESO AAAAAAAAAAA
        }
        String pagina;

        while (lectorArchivo.hasNext()) {
            pagina = lectorArchivo.next();
            String[] paginas = pagina.split("\\\\");

            int inicial = (paginas[0].charAt(0) == '\\') ? 0 : 1;

            for (int i=inicial; i<paginas.length; i+=2) {
                indiceAlfabetico.insert(paginas[i], numeroPagina);
            }
            numeroPagina++;
        }

        indiceAlfabetico.printIndice();

        String palabrita;
        do {
            System.out.println("Pulse enter para cerrar");
            System.out.println("Que palabra desea buscar?");
            System.out.print(">");
            palabrita = tcld.nextLine();
            if(!palabrita.isBlank()) {
                Index.Nodo nodito = indiceAlfabetico.busca(palabrita);
                if(nodito != null) {
                    nodito.print();
                    System.out.println();
                } else {
                    System.out.println("No se encuentra esa palabra >:(\n");
                }
            }
        } while (!palabrita.isBlank());
    }
}