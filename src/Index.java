// Beatriz Durán y Gonzalo Inostroza
public class Index {
    class Nodo { // Cada nodo representa un palabra
        String palabra;
        ListaPaginas paginas;
        Nodo iChild;
        Nodo dChild;

        public Nodo(String palabra, int pagina) {
            this.palabra = palabra;
            paginas = new ListaPaginas(pagina);
        }

        public void print() {
            System.out.print(palabra + " ");
            for (ListaPaginas.Pagina actual=paginas.head; actual !=null; actual=actual.siguiente) {
                if (actual.nVeces == 1) {
                    System.out.print(actual.num + " ");
                } else {
                    System.out.print(actual.num + "(" + actual.nVeces + ")" + " ");
                }
            }
            System.out.println();
        }
    }

    Nodo root;
    public Index() {
        root = null;
    }

    public void insert(String palabra, int pag) {
        if (root == null) {
            root = new Nodo(palabra, pag);
            return;
        }

        root = insertaRecursivo(palabra, pag, root);
    }

    private Nodo insertaRecursivo(String palabra, int pag, Index.Nodo nodito) {
        if (nodito == null) {
            return new Nodo(palabra, pag);
        }
        byte comparador = comparaPalabras(nodito.palabra, palabra);

        if (comparador == 1) { // Nodito es mayor
            nodito.dChild = insertaRecursivo(palabra, pag, nodito.dChild);
        } else if (comparador == -1) { // Palabra es mayor
            nodito.iChild = insertaRecursivo(palabra, pag, nodito.iChild);
        } else if (comparador == 0) {
            nodito.paginas.insertar(pag);
        }

        return nodito;
    }

    public Nodo busca(String palabra) {
        return buscaRecursivo(palabra, root);
    }

    private Nodo buscaRecursivo(String palabra, Nodo nodito) {
        if (nodito == null) {
            return null;
        }
        byte comparador = comparaPalabras(nodito.palabra, palabra);

        if (comparador == 1) { // Nodito es mayor
            return buscaRecursivo(palabra, nodito.dChild);
        } else if (comparador == -1) { // Palabra es mayor
            return buscaRecursivo(palabra, nodito.iChild);
        }

        return nodito;
    }


    public void printIndice() {
        printIndiceRecursivo(root, ' ');
    }

    private char printIndiceRecursivo(Nodo nodito, char ultimaLetra) {
        if (nodito == null) {
            return ultimaLetra;
        }
        ultimaLetra = printIndiceRecursivo(nodito.iChild, ultimaLetra);
        if (ultimaLetra != nodito.palabra.charAt(0)) {
            ultimaLetra = nodito.palabra.charAt(0);
            System.out.println("-"+ultimaLetra+"-");
        }
        nodito.print();
        ultimaLetra = printIndiceRecursivo(nodito.dChild, ultimaLetra);
        return ultimaLetra;
    }

    // Retorna 0 si son iguales, 1 si string1 es mayor, -1 si string2 es mayor
    private byte comparaPalabras(String string1, String string2) {
        string1 = string1.toLowerCase();
        string2 = string2.toLowerCase();
        if (string1.equals(string2)) {
            return 0;
        }
        char[] palabra1 = string1.toCharArray();
        char[] palabra2 = string2.toCharArray();

        int lenMin = Math.min(palabra1.length, palabra2.length);
        for (int i=0; i<lenMin; i++) {
            if (palabra1[i] > palabra2[i]) {
                return -1;
            } else if (palabra2[i] > palabra1[i]) {
                return 1;
            }
        }

        if (palabra1.length < palabra2.length) {
            return 1;
        }
        return -1;
    }
}
