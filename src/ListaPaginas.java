// Beatriz Durán y Gonzalo Inostroza
public class ListaPaginas {
    // Estos son los nodos, les puse pagina porque es más significativo
    class Pagina {
        int num; // Numero de la pagina
        int nVeces; // Cuantas veces sale en esa pagina
        Pagina siguiente;

        public Pagina(int p) {
            num = p;
            nVeces = 1;
            siguiente = null;
        }
    }

    Pagina head;
    Pagina ultimo;
    public ListaPaginas(int paginaInicial) {
        head = new Pagina(paginaInicial);
        ultimo = head;
    }

    void insertar(int p) {
        if (p == ultimo.num) {
            ultimo.nVeces++;
            return;
        }
        ultimo.siguiente = new Pagina(p);
        ultimo = ultimo.siguiente;
    }
}
